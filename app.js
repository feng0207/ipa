(function(window){
    var u = {};
    u.cb = null;
    apiready = function(){
        api.sendEvent({
            name: 'closeProgress'
        });
        api.hideProgress();
        //alert("hello world")
        setTimeout(function() {
            var header = document.getElementById("header");
            header.style.backgroundColor = "#209e85";
            u.fixStatusBar(header);
        }, 500)
        
       api.addEventListener({
           name: 'keyback'
       }, function(ret, err) {
           path = window.location.href
           reg = /#\/couse\/plan$/
           if(!reg.test(path)){
            window.history.back(-1); 
           } else {
            u.closeWin();
           }
       });
       typeof u.cb === "function" && u.cb();
    };

    u.calcHeight = function() {
        var sysType = api.systemType;
        if (sysType == 'ios') {
            return 20 + api.safeArea.top
        } else if (sysType == 'android') {
            var ver = api.systemVersion;
            ver = parseFloat(ver);
            if (ver >= 4.4) {
                return 25
            }
        }
    };

    u.isElement = function(obj){
        return !!(obj && obj.nodeType == 1);
    };
    u.fixIos7Bar = function(el){
        if(!u.isElement(el)){
            console.warn('fixIos7Bar Function need el param, el param must be DOM Element');
            return;
        }
        var strDM = api.systemType;
        if (strDM == 'ios') {
            var strSV = api.systemVersion;
            var numSV = parseInt(strSV,10);
            var fullScreen = api.fullScreen;
            var iOS7StatusBarAppearance = api.iOS7StatusBarAppearance;
            if (numSV >= 7 && !fullScreen && iOS7StatusBarAppearance) {
                el.style.paddingTop = '20px';
            }
        }
    };
    u.fixStatusBar = function(el){
        if(!u.isElement(el)){
            console.warn('fixStatusBar Function need el param, el param must be DOM Element');
            return;
        }
        var sysType = api.systemType;
        if(sysType == 'ios'){
            u.fixIos7Bar(el);
        }else if(sysType == 'android'){
            var ver = api.systemVersion;
            ver = parseFloat(ver);
            if(ver >= 4.4){
                el.style.paddingTop = '25px';
            }
        }
    };
    /**
     * 关闭当前窗口
     */
    u.closeWin = function() {
        if(api) {
            api.closeWin();
        }
    }
    u.closeFrame = function() {
        if(api) {
            api.closeFrame();
        }
    }

    /**
     * 设置屏幕旋转
     * @param {num} type 1 表示竖屏， 2 表示横屏
     */
    u.setScreenOrientation = function(type) {
        if(type === 1) {
            api.setScreenOrientation({
                orientation: 'portrait_up'
            });
        } else if(type === 2) {
            api.setScreenOrientation({
                orientation: 'auto_landscape'
            });
        }
    }
    /**
     * 是否屏幕保持常量
     * @param {boolean} flag true 表示屏幕常亮
     */
    u.keepScreen = function(flag) {
         api.setKeepScreenOn({
            keepOn: flag
            });
    }

    /**
     * 在登录页面获取登录参数
     * 返回值 
     * {
     *     "userName": "411425199502074518",
     *     "state"   : 1,
     *     "pwd"     : "123456",
     *     "area"    : "13202",
     *     "type"    : "cy"
     * }
     */
    u.getLoginParm = function() {
        return api.pageParam;
    }

    /**
     * 页面准备好之后的回调方法
     * @param {function} callback 
     */
    u.ready = function(callback) {
        if(typeof callback !== "function") {
            console.log("请传function类型的")
            return;
        }
        u.cb = callback;
    }

    window.rs = u;
})(window)