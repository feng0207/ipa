(function(window) {
    s = {}

    s.checkPermission = function() {
        var resultList = api.hasPermission({
            list:['camera']
        });
        return resultList;
    }

    s.requestPermission = function(callback) {
        var resultList = xyCamera.checkPermission()
        console.log(JSON.stringify(resultList))
        if(resultList[0].granted) {
            callback()
        }
        console.log("request")
        api.requestPermission({
            list:['camera'],
            code:1
        }, function(ret, err){
            if(!ret.list[0].granted) {
                api.alert({
                    title: '失败', 
                    msg: '申请相机权限失败，必须授权访问相机权限, 请重新打开本应用'
                });
            } else {
                callback()
            }
        });
    }

    s.openVideo = function (callback) {
        xyCamera.requestPermission(function() {
            api.getPicture({
                sourceType: 'camera',
                encodingType: 'jpg',
                destinationType: 'base64',
                allowEdit: false,
                quality: 100,
                targetWidth: 575,
                targetHeight: 575,
                saveToPhotoAlbum: false
            }, function (ret, err) {
                callback(ret, err);
            });
        })
        
    }

    window.xyCamera = s;
})(window)